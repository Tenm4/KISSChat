package client;

import java.util.Arrays;
import java.io.*;
import java.net.*;

public class ChatClient {
    
    public static String SERVER_IP;
    public static int CHAT_SERVER_PORT = 1111;
    public static int FILE_TRANSFER_PORT = 1112;
    
    private static final String SYSTEM_NAME = "SYSTEM";
    private static String ORIGIN_FORMAT = "<%s> %s";
    
    public static final String NSUP_CMD = "NSUP";
    public static final String DISP_CMD = "DISP";
    public static final String QUIT_CMD = "QUIT";
    public static final String LIST_CMD = "LIST";
    
    public static final String AUTH_CMD = "AUTH";
    public static final String USER_CMD = "USER";
    public static final String PASS_CMD = "PASS";
    
    public static final String FILE_TRANSFER_REQUEST_CMD = "FILQ";
    public static final String FILE_TRANSFER_RESPONSE_CMD = "FILR";
    public static final String FILE_SEND_CMD = "FILO";
    public static final String FILE_RECV_CMD = "FILI";

    private Socket socket;
    private PrintStream outputConsole, outputNetwork;
    private BufferedReader inputConsole, inputNetwork;

    private boolean quitted;
    private String login;
    private FileTransferThread fileTransferThread;

    public boolean isAuthenticated () {
        return this.login != null;
    }
    
    public ChatClient (String ip, int port) {
        this.quitted = false;
        this.login = null;
        this.fileTransferThread = null;
        
        initStreams(ip, port);
        try {
            new Thread(new ConsoleThread(), "Console").start();
            listenNetwork();
        } finally {
            quit();
        }
    }

    private void initStreams (String ip, int port) { 
        try {
            this.socket = new Socket(ip, port);
            this.inputNetwork = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.outputNetwork = new PrintStream(socket.getOutputStream());
            this.inputConsole = new BufferedReader(new InputStreamReader(System.in));
            this.outputConsole = System.out;
        }
        catch (UnknownHostException e1) {
            System.out.println("Unknown host !");
            System.exit(2);
        }
        catch (IOException e2) {
            System.out.println("Couldn't connect to host !");
            System.exit(3);
        }
    }

    private void listenConsole () {
        String input = "";

        try {
            while (!this.quitted) {
                //outputConsole.print("> "); // TODO: Display at bottom 
                input = inputConsole.readLine();
                //outputConsole.println("(NETWORK>: " + input + ")");
                this.processCommandConsole(input);
            }
        } catch (IOException e) {
            outputConsole.println("Connection interrupted with console.");
            e.printStackTrace();
        }
        //forwardString(inputConsole,outputNetwork,4, "Probleme dans la lecture de la console");
    }

    private void listenNetwork () {
        String input = "";

        try {
            while (!this.quitted) {
                input = this.inputNetwork.readLine();
                //outputConsole.println("(NETWORK<: " + input + ")");
                if (input == null) continue;
                try {
                    this.processCommandNetwork(input);
                } catch (ProtocolException e) { 
                    this.outputConsole.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            outputConsole.println("Connection interrupted with server.");
            e.printStackTrace();
        }
        //forwardString(inputNetwork,outputConsole,5, "Probleme dans la lecture du reseau. Le serveur est mort ?");
    }

    private boolean processCommandConsole (String input) { // TODO: file
        String cmd, src, dst, data, subTokens[];
        
        if (this.isAuthenticated() && input.startsWith("/")) {
            String[] tokens = input.substring(1).split(" ", 2);
            cmd = tokens[0];
        
            switch (cmd.toLowerCase()) {
                case "q": case "quit":
                    outputConsole.println("Disconnecting...");
                    this.send(QUIT_CMD, "");
                    this.quitted = true;
                break;
                case "list":
                    outputConsole.println("Listing users...");
                    this.send(LIST_CMD, "");
                break;
                case "msg":
                    subTokens = tokens[1].split(" ", 2);
                    if (subTokens.length != 2) {
                        outputConsole.printf("Usage: /%s <dest> <message>\n", cmd);
                        break;
                    }
                    dst = subTokens[0];
                    String msg = subTokens[1];
                    outputConsole.println(String.format("Sending message to %s...", dst));
                    this.send(DISP_CMD, this.login, dst, msg);
                break;
                case "file":
                    subTokens = tokens[1].split(" ", 2);
                    if (subTokens.length != 2) {
                        outputConsole.printf("Usage: /%s <dest> <file name>\n", cmd);
                        break;
                    }
                    if (this.fileTransferThread != null) {
                        outputConsole.println("Your are already transfering a file!"); 
                        break;
                    } 
                    dst = subTokens[0];
                    String filename = subTokens[1];
                    
                    if (!new File(filename).exists()) {
                        outputConsole.println("The requested file does not exists! Aborting transfer.");
                        break;
                    }
                    
                    this.fileTransferThread = new FileTransferThread(this.login, dst, filename, this.login);
                    outputConsole.printf("Negociating file transfer of file \"%s\" to %s...\n", filename, dst);
                    
                    File file = new File(filename);
                    this.send(FILE_TRANSFER_REQUEST_CMD, this.login, dst, String.format("%s %s", filename, file.length()));
                break;
                case "accept":
                    if (this.fileTransferThread == null) {
                        outputConsole.println("No file transfer has been requested!");
                        break;
                    }
                    if (!this.fileTransferThread.getDst().equals(this.login)) {
                        outputConsole.println("No file transfer has been registered for you!");
                    }
                    outputConsole.println("Accepted file transfer.");
                    new Thread(this.fileTransferThread, "File recv").start();
                    
                    this.send(FILE_TRANSFER_RESPONSE_CMD, this.login, this.fileTransferThread.getSrc(), String.format("%s %s OK", this.fileTransferThread.getFilename(), this.fileTransferThread.getFilesize()));
                break;
                case "cancel":
                    if (this.fileTransferThread == null) {
                        outputConsole.println("No file transfer has been requested!");
                        break;
                    }
                    if (!this.fileTransferThread.getDst().equals(this.login)) {
                        outputConsole.println("No file transfer has been registered for you!");
                    }
                    outputConsole.println("Canceled file transfer.");
                    
                    this.send(FILE_TRANSFER_RESPONSE_CMD, this.login, this.fileTransferThread.getSrc(), String.format("%s %s KO", this.fileTransferThread.getFilename(), 0));
                    
                    this.fileTransferThread = null;
                break;
                default:
                    outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, "Unknown command: " + input));
            }
        } else {
            this.send(input);
            //outputConsole.println(input);
        }
        // Command was processed
        return true;
    }
    
    private boolean processCommandNetwork (String input) throws ProtocolException {
        
        String[] tokens = decodeProto(input);
        String cmd = tokens[0], src = tokens[1], dst = tokens[2], data = tokens[3];
        
        switch (cmd) {
            case DISP_CMD:
                outputConsole.println(String.format(ORIGIN_FORMAT, src, data));
            break;
            case QUIT_CMD:
                if (data.length() > 0)
                    outputConsole.println(data);
            break;
            case USER_CMD:
                this.login = data;
            break;
            case LIST_CMD:
                outputConsole.println("Users: " + data);
            break;
            case FILE_TRANSFER_REQUEST_CMD: // TODO: impl + ask the user
                String[] negociationRequest = data.split(" ", 2);
                if (negociationRequest.length != 2) {
                    outputConsole.println("Invalid negociation request: expected 2 parameters");
                    break;
                }
                if (this.fileTransferThread != null) {
                    outputConsole.println("Your are already transfering a file!"); 
                    break;
                }
                String filename = negociationRequest[0], filesize = negociationRequest[1];
                this.fileTransferThread = new FileTransferThread(src, dst, filename, this.login);
                outputConsole.printf(ORIGIN_FORMAT, SYSTEM_NAME, String.format("%s wants to send you the file %s (size: %s bytes).\nType \"/accept\" to accept, or \"/cancel\" to cancel it.\n", src, filename, filesize));
                /*
                outputConsole.println("Accepted file transfer.");
                new Thread(this.fileTransferThread, "File recv").start();
                
                this.send(FILE_TRANSFER_RESPONSE_CMD, this.login, src, String.format("%s %s OK", filename, filesize)); 
                */
            break;
            case FILE_TRANSFER_RESPONSE_CMD:
                String[] negociationResponse = data.split(" ", 3);
                if (negociationResponse.length != 3) {
                    outputConsole.println("Invalid negociation response: expected 3 parameters");
                    break;
                }
                if (negociationResponse[2].equals("OK")) { // TODO: impl
                    outputConsole.println("Remote accepted file transfer.");
                    new Thread(this.fileTransferThread, "File send").start();
                } else {
                    outputConsole.println("Remote rejected file transfer.");
                }
            break;
            case NSUP_CMD:
                outputConsole.println("Command not implemented: /" + data);
            break;
            default:
                outputConsole.println("Unknown command: " + cmd);
        }
        // Command was processed
        return true;
    }

    private void quit () {
        outputConsole.println("*** Quitting ***");
        
        try {
            this.inputNetwork.close();
            this.outputNetwork.close();
            this.socket.close();
            //this.join();
        } catch (Exception e) { 
            outputConsole.println("Error in quit");
            e.printStackTrace();
        }
    }
    
    public void send (String cmd, String src, String dst, String data) {
        //try {
            this.outputNetwork.print(this.encodeProto(cmd, src, dst, data));
        //} catch (IOException e) { ChatServer.log.printf("Error in send: %s\n", msg); e.printStackTrace(); }
    }
    
    public void send (String cmd, String data) {
        this.send(cmd, this.login, "", data);
    }
    
    public void send (String msg) {
        this.send(DISP_CMD, this.login, "", msg);
    }
    
    public String encodeProto (String cmd, String src, String dst, String data) {
        return String.format("%s:%s:%s:%s\n", cmd, src, dst, data);
    }
    
    public String[] decodeProto (String input) throws ProtocolException { // TODO: (de)code using proto object
        String[] tokens = input.split(":", 4);
        //this.outputConsole.println("DEBUG: " + Arrays.toString(tokens));
        if (tokens.length != 4) {
            throw new ProtocolException(String.format("Protocol error: missing fields (%d,%s)\n", tokens.length, input)); // TODO: fix handling
        }
        return tokens;
    }
    
    
    class ConsoleThread implements Runnable {
    
        public void run () {
            listenConsole();
        }
    }
    
    
    class FileTransferThread implements Runnable {
        
        private String src;
        private String dst;
        private String filename;
        private int filesize;
        private String login; // login associated with file client
        
        public FileTransferThread (String src, String dst, String filename, String login) {
            this.src = src;
            this.dst = dst;
            this.login = login;
            this.filename = filename;
            this.filesize = (int)new File(filename).length();
        }
        
        public String getSrc () { return this.src; }
        public String getDst () { return this.dst; }
        public String getFilename () { return this.filename; }
        public int getFilesize () { return this.filesize; }
        public String getLogin () { return this.login; }
    
        public void run () {
            //System.out.println("FileTransferThread run()");
            Socket socket = null;
            OutputStreamWriter out = null;
            InputStream in = null;

            try {
                socket = new Socket(SERVER_IP, FILE_TRANSFER_PORT);
                
                out = new OutputStreamWriter(new BufferedOutputStream(socket.getOutputStream()));
                String data = String.format("%s:%s:%s:%s", this.src, this.dst, this.filename, this.login);
                out.write(data, 0, data.length());
                out.flush();
                
                in = socket.getInputStream();
                byte[] ba = new byte[10];
                in.read();
                
                if (this.login.equals(this.src)) {
                    this.sendFile(socket);
                } else if (this.login.equals(this.dst)) {
                    this.recvFile(socket);
                }
                
                in.read(ba, 0, 10);
                if (ba[0] == 0x31) {
                    if      (this.login.equals(this.src))
                        System.out.println("File send finished.");
                    else if (this.login.equals(this.dst))
                        System.out.println("File recieve finished.");
                }    
                
            } catch (UnknownHostException e1) {
                System.out.println("Unknown host!");
            } catch (FileNotFoundException e2) {
                System.out.println("File not found!");
            } catch (IOException e3) {
                System.out.println("Couldn't connect to file server! " + e3.getMessage());
            }
            finally {
                try {
                    if (in != null) in.close();
                    if (out != null) out.close();
                    if (socket != null) socket.close();
                } catch (IOException e) {}
                
                fileTransferThread = null;
            }
        }
        
        public void sendFile (Socket socket) throws FileNotFoundException, IOException {
            byte[] ba = new byte[this.filesize];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(this.filename));
            OutputStream os = socket.getOutputStream();
            
            bis.read(ba, 0, ba.length);
            os.write(ba, 0, ba.length);
            os.flush();
        }
        
        public void recvFile (Socket socket) throws IOException {
            byte[] ba = new byte[this.filesize];
            InputStream is = socket.getInputStream();
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(this.filename + ".cpy"));
                
            int bytesRead = is.read(ba, 0, ba.length);
            bos.write(ba, 0, bytesRead);
            bos.flush();
        }
    }
    
    
    public static void main (String[] args) {
        if (args.length != 2)
        {
            System.out.println("Usage : java ClientChat <server_ip> <port_number>");
            System.exit(1);
        }
        new ChatClient(args[0], Integer.parseInt(args[1]));
    }
}
