package client;

import java.util.Arrays;
import java.util.List;
import java.io.*;
import java.net.*;

import javax.xml.bind.DatatypeConverter;

import opencard.core.service.*;
import opencard.core.terminal.*;
import opencard.core.util.*;
import opencard.opt.util.*;

/*
TODO
- no crypto with server
- mark crypto usage
- file crypto
*/

public class TheClient {

	final static byte CLA                         = (byte)0x90;
	final static byte EMPTYP1                     = (byte)0x0;
	final static byte EMPTYP2                     = (byte)0x0;
	
	final static byte INS_GENERATE_RSA_KEY        = (byte)0xF6;
	final static byte INS_GET_PUBLIC_RSA_KEY      = (byte)0xFE;
	final static byte INS_PUT_PUBLIC_RSA_KEY      = (byte)0xF4;
	final static byte INS_RSA_ENCRYPT             = (byte)0xA0;
	final static byte INS_RSA_DECRYPT             = (byte)0xA2;
	final static byte INS_DES_ENCRYPT             = (byte)0xB0;
	final static byte INS_DES_DECRYPT             = (byte)0xB2;
	static final byte ENTER_PIN					  = (byte)0x01;
	static final byte UPDATE_PIN				  = (byte)0x02;
	
    private PassThruCardService servClient = null;
	boolean smartcardActivated = false;
    boolean DISPLAY = true;

	
	public boolean isSmartcardActivated () {
		return smartcardActivated;
	}
	
    private void initSmartcardService () throws QuittedException {
		outputConsole.println("Starting crypto service..."); 
		
	    try {
		    SmartCard.start();
		    System.out.print( "Smartcard inserted?... " ); 
		    
		    CardRequest cr = new CardRequest (CardRequest.ANYCARD,null,null); 
		    SmartCard sm = SmartCard.waitForCard (cr);
		   
		    if (sm != null) {
			    System.out.println("got a SmartCard object!\n");
		    } else
			    System.out.println("did not get a SmartCard object!\n");
		   
		    this.initNewCard(sm); 
	   
	    } catch (Exception e) {
		    //System.out.println( "TheClient error at init: " + e.getMessage() );
	    }
    }
	
	private void initNewCard( SmartCard card ) throws QuittedException {
		System.out.println( "ATR: " + HexString.hexify( card.getCardID().getATR() ) + "\n");

		try {
			this.servClient = (PassThruCardService)card.getCardService( PassThruCardService.class, true );
		} catch( Exception e ) {
			System.out.println( e.getMessage() );
		}

		System.out.println("Applet selecting...");
		if( !this.selectApplet() ) {
			System.out.println( "Wrong card, no applet to select!\n" );
			throw new QuittedException();
		} else 
			System.out.println( "Applet selected\n" );
		
		smartcardActivated = true;
		/*this.outputConsole.println("Please enter card PIN:");
		level = 5;*/
    }
	
	private void stopSmartcardService () {
		outputConsole.println("Stopping crypto service..."); 
		smartcardActivated = false;
		try {
			SmartCard.shutdown();
	    } catch( Exception e ) {
		    System.out.println( "TheClient error at shutdown: " + e.getMessage() );
	    }
	}

    private boolean selectApplet() {
		boolean cardOk = false;
		try {
			CommandAPDU cmd = new CommandAPDU(new byte[] {
                (byte)0x00, (byte)0xA4, (byte)0x04, (byte)0x00, (byte)0x0A,
				(byte)0xA0, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x62, 
				(byte)0x03, (byte)0x01, (byte)0x0C, (byte)0x06, (byte)0x01
            });
            ResponseAPDU resp = this.sendAPDU( cmd );
	    if( this.apdu2string( resp ).equals( "90 00" ) )
		    cardOk = true;
		} catch(Exception e) {
            System.out.println( "Exception caught in selectApplet: " + e.getMessage() );
            java.lang.System.exit( -1 );
        }
		return cardOk;
    }

	/************** BEGINNING OF TOOLS ****************/
	
	private String apdu2string( APDU apdu ) {
	    return removeCR( HexString.hexify( apdu.getBytes() ) );
    }

    public void displayAPDU( APDU apdu ) {
		System.out.println( removeCR( HexString.hexify( apdu.getBytes() ) ) + "\n" );
    }

    public void displayAPDU( CommandAPDU termCmd, ResponseAPDU cardResp ) {
		System.out.println( "--> Term: " + removeCR( HexString.hexify( termCmd.getBytes() ) ) );
		System.out.println( "<-- Card: " + removeCR( HexString.hexify( cardResp.getBytes() ) ) );
    }
	
    private String removeCR( String string ) {
	    return string.replace( '\n', ' ' );
    }
	
	private ResponseAPDU sendAPDU(CommandAPDU cmd) {
	    return sendAPDU(cmd, true);
    }

    private ResponseAPDU sendAPDU( CommandAPDU cmd, boolean display ) {
	    ResponseAPDU result = null;
	    try {
			if (display)
				System.out.printf("--> Term: (%s) %s\n", cmd.getBytes().length, apdu2string(cmd));
			result = this.servClient.sendCommandAPDU( cmd );
			if (display)
				System.out.printf("<-- Card: (%s) %s\n", result.getBytes().length, apdu2string(result));
	    } catch( Exception e ) {
           	System.out.println( "Exception caught in sendAPDU: " + e.getMessage() );
           	java.lang.System.exit( -1 );
        }
	    return result;
    }

	byte[] sendRecvCard(byte ins, byte p1, byte p2, short dataLength, byte[] data, short le) {
		byte[] apduHeader;
		if (dataLength != 0)
			apduHeader = new byte[] {CLA, ins, p1, p2, (byte)dataLength};
		else apduHeader = new byte[] {CLA, ins, p1, p2};
		//System.out.println("Building APDU...");
		byte[] cmdBytes = new byte[4 + (dataLength > 0 ? 1 : 0) + dataLength + (le >= 0 ? 1 : 0)];
		System.arraycopy(apduHeader, (byte)0, cmdBytes, (byte)0, 4 + (dataLength > 0 ? 1 : 0));
		if (dataLength != 0) {
			//System.out.println("Adding data: (" + (data.length+6)  + ") " + HexString.hexify(data));
			System.arraycopy(data, (byte)0, cmdBytes, (byte)5, dataLength);
		} if (le >= 0)
			cmdBytes[cmdBytes.length - 1] = (byte)le;
		byte[] res = this.sendAPDU(new CommandAPDU(cmdBytes), DISPLAY).getBytes();
		return res;
	}
	
	byte[] sendRecvCard(byte ins, byte[] data, short le) {
		return sendRecvCard(ins, EMPTYP1, EMPTYP2, (short)data.length, data, le);
	}
	
	byte[] sendRecvCard(byte ins, byte[] data) {
		return sendRecvCard(ins, data, (byte)-1);
	}
	
	byte[] askUser (String message) {
		String input = "";
		System.out.println(message);
		try {
			do {
				input = this.inputConsole.readLine();
			} while (input.length() > 0);
		} catch (IOException e) {}
		return input.getBytes();
	}
	
	/************** END OF TOOLS ***************/
	
	boolean verifyPIN(String input) {
		byte[] res = sendRecvCard(ENTER_PIN, input.getBytes());
		//System.out.println("Pin status: " + HexString.hexify(res));
		return res[0] == 0x90;
	}
	
	void updatePIN(String input) {
		sendRecvCard(UPDATE_PIN, input.getBytes());
	}
	
	private byte[] retrieveRSAPublicKey () {
		short BLOCK_SIZE = 128, MAX_SIZE = 240;
		byte[] block = new byte[BLOCK_SIZE], rsaData = new byte[2 + 3 + BLOCK_SIZE];
		
		block = sendRecvCard(INS_GET_PUBLIC_RSA_KEY, EMPTYP1, (byte)1, (short)0, null, (short)4);
		rsaData[0] = (byte)3; rsaData[1] = (byte)BLOCK_SIZE;
		System.arraycopy(block, 1, rsaData, 2, 3);
		
		block = sendRecvCard(INS_GET_PUBLIC_RSA_KEY, EMPTYP1, (byte)0, (short)0, null, (short)(BLOCK_SIZE+1));
		System.arraycopy(block, 1, rsaData, 2 + 3, BLOCK_SIZE);
		
		return rsaData;
	}
	
	private byte[] resolveChallenge (byte[] challenge) {
		byte[] res = sendRecvCard(INS_RSA_DECRYPT, challenge, (short)challenge.length);	
		return Arrays.copyOfRange(res, 0, challenge.length); // Get rid of the 2 status bytes
	}
	
	public byte[] cryptDES (byte[] nopadInput, byte cipherMode) {
		final short BLOCK_SIZE = 240;
		if (nopadInput.length >= BLOCK_SIZE) this.outputConsole.println("\n/!\\ Buffer too long!\n");
		
		/*byte[] buf = new byte[] {CLA, INS_GET_PUBLIC_RSA_KEY, EMPTYP1, EMPTYP2, (byte)2};
		System.out.println("Test: " + HexString.hexify(buf) + " -> " +
			HexString.hexify(this.sendAPDU(new CommandAPDU(buf), DISPLAY).getBytes()));*/
		
		/*int blockNo, length = nopadInput.length;
		length += (length % 8 == 0 ? 0 : (8 - length % 8)); // Padding %8
		byte[] input = new byte[length], output = new byte[length], buffer = new byte[BLOCK_SIZE];
		
		System.arraycopy(nopadInput, 0, input, 0, nopadInput.length);
		System.out.println("Input: " + HexString.hexify(input));
		buffer = sendRecvCard(cipherMode, EMPTYP1, EMPTYP2, (short)length, input, (short)length);
		if (buffer.length >= 2 && buffer[buffer.length - 2] != (byte)0x90)
			System.out.println("Normal exception:");
		System.arraycopy(buffer, 0, output, 0, length);
		System.out.println("Output: " + HexString.hexify(output));
		
		return output;*/
		
		int blockNo, length = nopadInput.length;
		length += (length % 8 == 0 ? 0 : (8 - length % 8)); // Padding %8
		byte[] buffer = new byte[BLOCK_SIZE], input = new byte[length], output = new byte[length];
		
		//System.out.println("Buffer 1: " + HexString.hexify(nopadInput));
		//System.out.println("Lengths: " + length +" "+ BLOCK_SIZE);
		System.arraycopy(nopadInput, 0, input, 0, nopadInput.length);
		
		// Send all but the last data block
		for (blockNo = 0; blockNo < length / BLOCK_SIZE; blockNo++) {
			System.arraycopy(input, blockNo * BLOCK_SIZE, buffer, 0, BLOCK_SIZE);
			buffer = sendRecvCard(cipherMode, EMPTYP1, EMPTYP2, BLOCK_SIZE, buffer, BLOCK_SIZE);
			//if (buffer.length >= 2 && buffer[buffer.length - 2] != (byte)0x90) return null;
			System.arraycopy(buffer, 0, output, blockNo * BLOCK_SIZE, BLOCK_SIZE);
		}
		
		// Send the last data block
		short remainingSize = (short)(length % BLOCK_SIZE);
		remainingSize += (short)(remainingSize % 8 == 0 ? 0 : (8 - remainingSize % 8));
		//System.out.println("remainingSize: " + remainingSize);
		buffer = new byte[remainingSize];//Arrays.fill(buffer, (byte)0);
		
		System.arraycopy(input, blockNo * BLOCK_SIZE, buffer, 0, remainingSize);
		//System.out.println("Buffer 2: " + HexString.hexify(buffer));
		buffer = sendRecvCard(cipherMode, EMPTYP1, EMPTYP2, remainingSize, buffer, remainingSize);
		System.arraycopy(buffer, 0, output, blockNo * BLOCK_SIZE, remainingSize);
		
		return output;
	}
	
	
	///////////////////////// END SMARTCARD CODE /////////////////////////////
	
	
	public static String SERVER_IP;
    public static int CHAT_SERVER_PORT = 1111;
    public static int FILE_TRANSFER_PORT = 1112;
    
    private static final String SYSTEM_NAME = "SYSTEM";
    private static String ORIGIN_FORMAT = "<%s> %s";
    
    public static final String NSUP_CMD = "NSUP";
	public static final String QUIT_CMD = "QUIT";
	
    public static final String DISP_CMD = "DISP";
    public static final String LIST_CMD = "LIST";
    
    public static final String AUTH_TYPE_CMD = "AUTT";
	public static final String AUTH_STATUS_CMD = "AUTS";
    public static final String USER_CMD = "USER";
    public static final String PASS_CMD = "PASS";
	public static final String SMARTCARD_USE_CMD = "CARD";
	public static final String RSA_PUBKEY_CMD = "PKEY";
	public static final String RSA_CHALLENGE_CMD = "CHAL";
    
    public static final String FILE_TRANSFER_REQUEST_CMD = "FILQ";
    public static final String FILE_TRANSFER_RESPONSE_CMD = "FILR";
    public static final String FILE_SEND_CMD = "FILO";
    public static final String FILE_RECV_CMD = "FILI";

    private Socket socket;
    private PrintStream outputConsole, outputNetwork;
    private BufferedReader inputConsole, inputNetwork;

    private boolean quitted;
    private String login;
    private FileTransferThread fileTransferThread;
	
	int level;

    public boolean isAuthenticated () {
        return this.login != null;
    }
    
    public TheClient () {
		
		String ip = "127.0.0.1"; 
        this.quitted = false;
        this.login = null;
        this.fileTransferThread = null;
		this.level = 0;
        
		this.initStreams(ip, CHAT_SERVER_PORT);
		try {
            new Thread(new ConsoleThread(), "Console").start();
            listenNetwork();
		} catch (QuittedException e) {
			this.outputConsole.println("QuittedException");
        } finally {
			level = -1;
            this.quit();
			if (isSmartcardActivated())
				this.stopSmartcardService();
			System.exit(0);
        }
    }

    private void initStreams (String ip, int port) {
        try {
            this.socket = new Socket(ip, port);
            this.inputNetwork = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.outputNetwork = new PrintStream(socket.getOutputStream());
            this.inputConsole = new BufferedReader(new InputStreamReader(System.in));
            this.outputConsole = System.out;
        }
        catch (UnknownHostException e1) {
            System.out.println("Unknown host !");
            System.exit(2);
        }
        catch (IOException e2) {
            System.out.println("Couldn't connect to host !");
            System.exit(3);
        }
    }

    private void listenConsole () throws QuittedException {
        String input = "";

        //try {
            while (!this.quitted) {
                //outputConsole.print("> "); // TODO: Display at bottom 
                input = this.recvConsole();
                //outputConsole.println("(NETWORK>: " + input + ")");
                this.processCommandConsole(input);
            }
		/*} catch (IOException e) {
            outputConsole.println("Connection interrupted with console.");
            e.printStackTrace();
        }*/
    }

    private void listenNetwork () throws QuittedException {
        String[] input = null;

        //try {
            while (!this.quitted) {
				try {
					input = this.recv();
				} catch (ProtocolException e) { 
                    this.outputConsole.println(e.getMessage());
                }
                //outputConsole.println("(NETWORK<: " + input + ")");
                this.processCommandNetwork(input);
            }
        /*} catch (IOException e) {
            outputConsole.println("Connection interrupted with server.");
            e.printStackTrace();
        }*/
    }

    private boolean processCommandConsole (String input) throws QuittedException {
        String cmd, src, dst, data, subTokens[];
        
		/*if (this.level == 5) {
			if (!this.verifyPIN(input)) {
				outputConsole.println("Smartcard pin verification failed. Aborting...");
				throw new QuittedException();
			}
			smartcardActivated = true;
			level = 6;
		}*/
		
        if (this.isAuthenticated() && input.startsWith("/")) {
            String[] tokens = input.substring(1).split(" ", 2);
            cmd = tokens[0].toLowerCase();
			
            if (cmd.equals("q") || cmd.equals("quit")) {
				outputConsole.println("Disconnecting...");
				this.send(QUIT_CMD, login, SYSTEM_NAME, "");
				this.quitted = true;
            }
			else if (cmd.equals("list")) {
				outputConsole.println("Listing users...");
				this.send(LIST_CMD, login, SYSTEM_NAME, "");
            }
			else if (cmd.equals("msg")) {
				if (tokens.length != 2) {
					outputConsole.printf("Usage: /%s <dest> <file name>\n", cmd);
				} else {
				subTokens = tokens[1].split(" ", 2);
				if (subTokens.length != 2) {
					outputConsole.printf("Usage: /%s <dest> <message>\n", cmd);
				} else {
					dst = subTokens[0];
					String msg = subTokens[1];
					outputConsole.println(String.format("Sending message to %s...", dst));
					this.send(DISP_CMD, this.login, dst, msg);
				}
				}
            }
			else if (cmd.equals("file")) {
				if (tokens.length != 2) {
					outputConsole.printf("Usage: /%s <dest> <file name>\n", cmd);
				} else {
				subTokens = tokens[1].split(" ", 2);
				if (subTokens.length != 2) {
					outputConsole.printf("Usage: /%s <dest> <file name>\n", cmd);
				} else {
				if (this.fileTransferThread != null) {
					outputConsole.println("Your are already transfering a file!"); 
				} else {
					dst = subTokens[0];
					String filename = subTokens[1];
					File file = new File(filename);
					
					if (!file.exists() || !file.isFile()) {
						outputConsole.println("The requested file does not exists! Aborting transfer.");
					} else {
					
					this.fileTransferThread = new FileTransferThread(this.login, dst, filename, this.login);
					outputConsole.printf("Negociating file transfer of file \"%s\" to %s...\n", filename, dst);
					
					String ftPayload = String.format("%s %s", filename, file.length());
					outputConsole.printf("FT Request Payload: %s %s\n", filename, file.length());
					this.send(FILE_TRANSFER_REQUEST_CMD, this.login, dst, ftPayload);
				}
				}
				}
				}
			}
			else if (cmd.equals("accept")) {
				if (this.fileTransferThread == null) {
					outputConsole.println("No file transfer has been requested!");
				} else {
				if (!this.fileTransferThread.getDst().equals(this.login)) {
					outputConsole.println("No file transfer has been registered for you!");
				}
				outputConsole.println("Accepted file transfer.");
				new Thread(this.fileTransferThread, "File recv").start();
				
				this.send(FILE_TRANSFER_RESPONSE_CMD, this.login, this.fileTransferThread.getSrc(), String.format("%s %s OK", this.fileTransferThread.getFilename(), this.fileTransferThread.getFilesize()));
				}
			}
			else if (cmd.equals("cancel")) {
				if (this.fileTransferThread == null) {
					outputConsole.println("No file transfer has been requested!");
				} else {
				if (!this.fileTransferThread.getDst().equals(this.login)) {
					outputConsole.println("No file transfer has been registered for you!");
				} else {
				outputConsole.println("Canceled file transfer.");
				
				this.send(FILE_TRANSFER_RESPONSE_CMD, this.login, this.fileTransferThread.getSrc(), String.format("%s %s KO", this.fileTransferThread.getFilename(), 0));
				
				this.fileTransferThread = null;
				}
				}
			}
			else if (cmd.equals("crypto")) {
				if (tokens.length != 2) {
					outputConsole.printf("Usage: /%s 0|1\n", cmd);
				} else {
					if (tokens[1].equals("1")) this.initSmartcardService();
					else if (tokens[1].equals("0")) this.stopSmartcardService();
					else outputConsole.printf("Usage: /%s 0|1\n", cmd);
				}
            } else {
                outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, "Unknown command: " + input));
            }
        } else {
            this.send(input);
            //outputConsole.println(input);
        }
        // Command was processed
        return true;
    }
    
    private boolean processCommandNetwork (String[] input) throws QuittedException {
        
        String cmd = input[0], src = input[1], dst = input[2], data = input[3];
        
        if (cmd.equals(DISP_CMD)) {
			outputConsole.println(String.format(ORIGIN_FORMAT, src, data));
        }
		else if (cmd.equals(LIST_CMD)) {
			outputConsole.println("Users: " + data);
        }
		else if (cmd.equals(QUIT_CMD)) {
			if (data.length() > 0)
				outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, data));
			throw new QuittedException();
        }
		else if (cmd.equals(AUTH_TYPE_CMD)) {
			level = 2;
			outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, 
				"Choose an authentication method: " + joinStrings(data.split(","), " or ", 0)));
		}
		else if (cmd.equals(AUTH_STATUS_CMD)) {
			String[] status = data.split(",", 2);
			if (status[0].equals("METHOD")) {
				outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, "Autentication method: " + status[1]));
				level = 3;
			}
			if (status[0].equals("SUCCESS")) {
				this.login = status[1];
				level = 10;
			}
        }
		else if (cmd.equals(USER_CMD)) {
			outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, "Please enter username:"));
		}
		else if (cmd.equals(PASS_CMD)) {
			outputConsole.println(String.format(ORIGIN_FORMAT, SYSTEM_NAME, "Please enter password:"));
		}
		else if (cmd.equals(SMARTCARD_USE_CMD)) {
			outputConsole.println("Please authenticate via smartcard:");
			this.initSmartcardService();
			if (isSmartcardActivated())
				this.send(SMARTCARD_USE_CMD, login, SYSTEM_NAME, "OK");
		}
		else if (cmd.equals(RSA_PUBKEY_CMD)) {
			if (isSmartcardActivated()) {
				outputConsole.println("Retrieving and sending public key..."); 
				byte[] pubKeyData = retrieveRSAPublicKey();
				outputConsole.println("Public key: " + DatatypeConverter.printHexBinary(pubKeyData) + "\n");
				this.send(RSA_PUBKEY_CMD, login, SYSTEM_NAME, DatatypeConverter.printBase64Binary(pubKeyData));
			}
		}
		else if (cmd.equals(RSA_CHALLENGE_CMD)) {
			if (isSmartcardActivated()) {
				outputConsole.println("Solving challenge requested by server..."); 
				//outputConsole.println("Challenge request : " + data); 
				byte[] solution = resolveChallenge(DatatypeConverter.parseBase64Binary(data));
				//outputConsole.println("Challenge response: " + Arrays.toString(solution) + "\n" + DatatypeConverter.printBase64Binary(solution));
				this.send(RSA_CHALLENGE_CMD, login, SYSTEM_NAME, DatatypeConverter.printBase64Binary(solution));
			}
		}
		else if (cmd.equals(FILE_TRANSFER_REQUEST_CMD)) {
			String[] negociationRequest = data.split(" ", 2);
			if (negociationRequest.length != 2) {
				outputConsole.println("Invalid negociation request: expected 2 parameters");
			} else {
				if (this.fileTransferThread != null) {
					outputConsole.println("Your are already transfering a file!"); 
				} else {
					String filename = negociationRequest[0], filesize = negociationRequest[1];
					this.fileTransferThread = new FileTransferThread(src, dst, filename, this.login);
					outputConsole.printf(ORIGIN_FORMAT, SYSTEM_NAME, String.format("%s wants to send you the file %s (size: %s bytes).\nType \"/accept\" to accept, or \"/cancel\" to cancel it.\n", src, filename, filesize));
				}
			}
        }
		else if (cmd.equals(FILE_TRANSFER_RESPONSE_CMD)) {
			String[] negociationResponse = data.split(" ", 3);
			if (negociationResponse.length != 3) {
				outputConsole.println("Invalid negociation response: expected 3 parameters");
			} else {
				if (negociationResponse[2].equals("OK")) {
					outputConsole.println("Remote accepted file transfer.");
					new Thread(this.fileTransferThread, "File send").start();
				} else {
					outputConsole.println("Remote rejected file transfer.");
				}
			}
		}
		else if (cmd.equals(NSUP_CMD)) {
			outputConsole.println("Command not implemented: /" + data);
        } else {
            outputConsole.println("Unknown command: " + cmd);
        }
        // Command was processed
        return true;
    }
	
    private void quit () {
        outputConsole.println("*** Quitting ***");
        
        try {
            this.inputNetwork.close();
            this.outputNetwork.close();
            this.socket.close();
            //this.join();
        } catch (Exception e) { 
            outputConsole.println("Error in quit");
            e.printStackTrace();
        }
    }
    
    public void send (String cmd, String src, String dst, String data) {
        try {
			if (this.isSmartcardActivated() && !dst.equals(SYSTEM_NAME))
				data = "#" + DatatypeConverter.printBase64Binary(cryptDES(data.getBytes("UTF-8"), INS_DES_ENCRYPT));
            this.outputNetwork.print(this.encodeProto(cmd, src, dst, data));
		} catch (UnsupportedEncodingException e) { outputConsole.printf("Encoding error in send: %s\n", data); e.printStackTrace(); }
        //} catch (IOException e) { outputConsole.printf("Error in send: %s\n", data); e.printStackTrace(); }
    }
    
    public void send (String cmd, String data) {
        this.send(cmd, this.login, "", data);
    }
    
    public void send (String msg) {
        this.send(DISP_CMD, this.login, "", msg);
    }
	
	public String recvConsole () throws QuittedException {
        String data = null;
        try {
            data = inputConsole.readLine();
        } catch (IOException e) { this.outputConsole.println("Error in recv"); /*e.printStackTrace();*/ }
        if (data == null)
            throw new QuittedException();
        return data;
    }
	
	public String[] recv () throws QuittedException, ProtocolException {
        String[] tokens = null;
        try {
            tokens = this.decodeProto(inputNetwork.readLine()); // TODO: fix Ctrl+C (SIGPIPE socket broken)
			if (tokens[3].length() > 0 && tokens[3].charAt(0) == '#') {
				if (this.isSmartcardActivated() && this.isAuthenticated())
					tokens[3] = new String(cryptDES(DatatypeConverter.parseBase64Binary(tokens[3]), INS_DES_DECRYPT), "UTF-8");
				else this.outputConsole.println("Recieved encrypted message. Enable crypto to read this kind of message.");
			}
		} catch (IOException e) { this.outputConsole.println("Error in recv"); /*e.printStackTrace();*/ }
        if (tokens == null)
            throw new QuittedException();
        return tokens;
    }
    
    public String encodeProto (String cmd, String src, String dst, String data) {
		//this.outputConsole.printf("PROTO DEBUG: %s:%s:%s:%s\n", cmd, src, dst, data);
        return String.format("%s:%s:%s:%s\n", cmd, src, dst, data);
    }
    
    public String[] decodeProto (String input) throws ProtocolException { // TODO: (de)code using proto object
        String[] tokens = input.split(":", 4);
        //this.outputConsole.println("PROTO DEBUG: " + Arrays.toString(tokens));
        if (tokens.length != 4) {
            throw new ProtocolException(String.format("Protocol error: missing fields (%d,%s)\n", tokens.length, input)); // TODO: fix handling
        }
        return tokens;
    }
	
	
	private static String joinStrings (String[] strings, String separator, int startIndex) {
		StringBuffer sb = new StringBuffer();
		for (int i = startIndex; i < strings.length; i++) {
			if (i != startIndex) sb.append(separator);
			sb.append(strings[i]);
		}
		return sb.toString();
	}
    
    
    class ConsoleThread implements Runnable {
    
        public void run () {
            try {
				listenConsole();
			} catch (QuittedException e) {
				outputConsole.println("QuittedException");
			}
        }
    }
    
    
    class FileTransferThread implements Runnable {
        
        private String src;
        private String dst;
        private String filename;
        private int filesize;
        private String login; // login associated with file client
        
        public FileTransferThread (String src, String dst, String filename, String login) {
            this.src = src;
            this.dst = dst;
            this.login = login;
            this.filename = filename;
            this.filesize = (int)new File(filename).length();
        }
        
        public String getSrc () { return this.src; }
        public String getDst () { return this.dst; }
        public String getFilename () { return this.filename; }
        public int getFilesize () { return this.filesize; }
        public String getLogin () { return this.login; }
    
        public void run () {
            //System.out.println("FileTransferThread run()");
            Socket socket = null;
            OutputStreamWriter out = null;
            InputStream in = null;

            try {
                socket = new Socket(SERVER_IP, FILE_TRANSFER_PORT);
                
                out = new OutputStreamWriter(new BufferedOutputStream(socket.getOutputStream()));
                String data = String.format("%s:%s:%s:%s", this.src, this.dst, this.filename, this.login);
                out.write(data, 0, data.length());
                out.flush();
                
                in = socket.getInputStream();
                byte[] ba = new byte[10];
                in.read();
                
                if (this.login.equals(this.src)) {
                    this.sendFile(socket);
                } else if (this.login.equals(this.dst)) {
                    this.recvFile(socket);
                }
                
                in.read(ba, 0, 10);
                if (ba[0] == 0x31) {
                    if (this.login.equals(this.src))
                        System.out.println("File send finished.");
                    else if (this.login.equals(this.dst))
                        System.out.println("File recieve finished.");
                }    
                
            } catch (UnknownHostException e1) {
                System.out.println("Unknown host: " + SERVER_IP);
            } catch (FileNotFoundException e2) {
                System.out.println("File not found: " + this.filename);
            } catch (IOException e3) {
                System.out.println("Couldn't connect to file server! " + e3.getMessage());
            }
            finally {
                try {
                    if (in != null) in.close();
                    if (out != null) out.close();
                    if (socket != null) socket.close();
                } catch (IOException e) {}
                
                fileTransferThread = null;
            }
        }
        
        public void sendFile (Socket socket) throws FileNotFoundException, IOException {
            byte[] ba = new byte[this.filesize];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(this.filename));
            OutputStream os = socket.getOutputStream();
            
			if (isSmartcardActivated())
				ba = cryptDES(ba, TheClient.INS_DES_ENCRYPT);
			
            bis.read(ba, 0, ba.length);
            os.write(ba, 0, ba.length);
            os.flush();
        }
        
        public void recvFile (Socket socket) throws IOException {
            byte[] ba = new byte[this.filesize];
            InputStream is = socket.getInputStream();
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(this.filename + ".cpy"));
            
			if (isSmartcardActivated())
				ba = cryptDES(ba, TheClient.INS_DES_DECRYPT);
			
            int bytesRead = is.read(ba, 0, ba.length);
            bos.write(ba, 0, bytesRead);
            bos.flush();
        }
    }

    public static void main( String[] args ) throws InterruptedException {
	    new TheClient();
    }
}

class QuittedException extends Exception {}
class ProtocolException extends Exception {
    public ProtocolException (String message) {
        super(message);
    }
}
