package server;


public class User {

    private String login;
    private String password;
    private String authToken;
    private String[] authMethods;
    private boolean authenticated;

    public User (String login) {
        this.login = login;
        this.password = null;
        this.authToken = null;
        this.authMethods = new String[]{"null", "password", "smartcard"};
        this.authenticated = false;
    }

    public String getLogin () {
        return this.login;
    }

    public String getPassword () {
        return this.password;
    }
    
    public String getAuthToken () {
        return this.authToken;
    }
    
    public String[] getAuthMethods () {
        return this.authMethods;
    }

    public boolean isAuthenticated () {
        return this.authenticated;
    }
    
	public synchronized boolean setPassword (String password) {
        if (this.password == null) {
			this.password = password;
			return true;
		}
		return false;
    }

	public synchronized boolean setAuthToken (String authToken) {
        if (this.authToken == null) {
			this.authToken = authToken;
			return true;
		}
		return false;
    }
	
    public synchronized boolean setAuthenticated (boolean auth) {
        if (auth != this.authenticated) {
			this.authenticated = auth;
			return true;
		}
		return false;
    }
}
