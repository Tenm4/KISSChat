package server;

import java.util.ArrayList;
import java.net.*;
import java.io.*;
import java.nio.*;


public class ChatService extends Thread {

    private static ArrayList<ChatService> serviceWorkers = new ArrayList<>(ChatServer.MAX_SIMULTANEOUS_CLIENTS);
    
    public static final String SYSTEM_NAME = "SYSTEM";
    
    public static final String NSUP_CMD = "NSUP";
	public static final String QUIT_CMD = "QUIT";
	
    public static final String DISP_CMD = "DISP";
    public static final String LIST_CMD = "LIST";
    
    public static final String AUTH_TYPE_CMD = "AUTT";
	public static final String AUTH_STATUS_CMD = "AUTS";
    public static final String USER_CMD = "USER";
    public static final String PASS_CMD = "PASS";
	public static final String SMARTCARD_USE_CMD = "CARD";
	public static final String RSA_PUBKEY_CMD = "PKEY";
	public static final String RSA_CHALLENGE_CMD = "CHAL";
    
    public static final String FILE_TRANSFER_REQUEST_CMD = "FILQ";
    public static final String FILE_TRANSFER_RESPONSE_CMD = "FILR";
    public static final String FILE_SEND_CMD = "FILO";
    public static final String FILE_RECV_CMD = "FILI";

    public static boolean isFull () {
        return ChatService.serviceWorkers.size() > ChatServer.MAX_SIMULTANEOUS_CLIENTS;
    }
    
    public static boolean routeData (String cmd, String src, String dst, String data) {
        if (dst.length() > 0) { // unicast
            if (ChatServer.authenticationService.getAuthenticatedLogins().contains(dst)) {
                for (ChatService worker : serviceWorkers) {
                    if (worker.userLogin.equals(dst)) {
                        worker.send(cmd, src, dst, data);
                        return true;
                    }
                }
            }
        } else { // broadcast
            for (ChatService worker : serviceWorkers) {
                worker.send(cmd, src, worker.userLogin, data);
            }
            return true;
        }
        return false; // routing failed
    }
    
    private Socket socket;
    private BufferedReader input;
    private PrintStream output;
    
    private String userLogin;

    public ChatService (Socket sock) throws IOException {
        this.socket = sock;
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.output = new PrintStream(socket.getOutputStream());
    }
    
    public void run () {
        serviceWorkers.add(this);
        ChatServer.log.printf("Worker started! #workers: %d\n", serviceWorkers.size());
        try {
            init();
            mainloop();
        } catch (QuittedException e) {
			ChatServer.log.println("QuittedException");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.quit();
        serviceWorkers.remove(this);
        ChatServer.log.printf("Worker stopped! #workers: %d\n", serviceWorkers.size());
    }
    
    private void init() throws QuittedException {
        
        if (ChatService.isFull()) {
            this.send("Cannot connect. Server is full.");
            throw new QuittedException();
        } else {
            this.send("Hello from server!");
        }
        
        this.userLogin = ChatServer.authenticationService.authenticate(this, null);
        this.send("Connected users: " + this.listUsers());
    }

    private void mainloop () throws QuittedException {
        String data;

        while (true) {
            data = this.recv();
            processCommand(data);
        }
    }

    public void send (String cmd, String src, String dst, String data) {
        //try {
            this.output.print(String.format("%s:%s:%s:%s\n", cmd, src, dst, data));
        //} catch (IOException e) { ChatServer.log.printf("Error in send: %s\n", msg); e.printStackTrace(); }
    }
    
    public void send (String cmd, String data) {
        this.send(cmd, SYSTEM_NAME, "", data);
    }
    
    public void send (String msg) {
        this.send(DISP_CMD, SYSTEM_NAME, "", msg);
    }

    public String recv () throws QuittedException {
        String data = null;
        try {
            data = this.input.readLine(); // TODO: fix Ctrl+C (SIGPIPE socket broken)
        } catch (IOException e) { ChatServer.log.println("Error in recv"); /*e.printStackTrace();*/ }
        if (data == null)
            throw new QuittedException();
        return data;
    }
    
    public String recvChat() throws QuittedException {
        String msg = this.recv();
        return decodeProto(msg)[3];
    }

    public String[] decodeProto(String input) { // TODO: (de)code using proto object
        /* Specification:
         * 4 textual fields, separated by a colon
         * cmd: server/client command, must not be void
         * src: source of the communication, must not be void
         * dst: destination of the communication, void for all (broadcast)
         * data: payload data, can be void
         */
        
        String[] tokens = input.split(":", 4);
        //ChatServer.log.println("DEBUG: " + Arrays.toString(tokens));
        if (tokens.length != 4) {
            try { 
                throw new ProtocolException("Missing fields"); // TODO: fix handling?
            } catch (ProtocolException e) { ChatServer.log.printf("Protocol error: missing fields (%d,%s)\n", tokens.length, input); tokens = new String[]{"", "", ""}; }
        }
        return tokens;
    }

    private boolean processCommand (String input) throws QuittedException {
        String[] tokens = this.decodeProto(input);
        // src and dst are trusted
        String cmd = tokens[0], src = tokens[1], dst = tokens[2], data = tokens[3], filename;
        
        switch (cmd) {
			case QUIT_CMD:
                this.send(QUIT_CMD, "");
                throw new QuittedException();
            case DISP_CMD:
                if (!ChatService.routeData(cmd, src, dst, data)) {
                    this.send(DISP_CMD, SYSTEM_NAME, src, "Destination user is not connected!");
                }
            break;
            case LIST_CMD:
                this.send(LIST_CMD, SYSTEM_NAME, "", this.listUsers());
            break;
            case FILE_TRANSFER_REQUEST_CMD:
                String[] reqData = data.split(" ", 2);
				
				if (reqData.length != 2) {
					ChatServer.log.printf("Incorrect file transfer request\n", cmd);
				} else {
					filename = reqData[0];
					int filesize = Integer.parseInt(reqData[1]);
					if (FileTransferServer.registerTransfer(src, dst, filename, filesize)) {
						if (!ChatService.routeData(cmd, src, dst, data)) {
							this.send(DISP_CMD, SYSTEM_NAME, src, "Destination user is not connected!");
						}
					} else {
						ChatServer.log.println("Invalid transfer parameters! Aborted file transfer.");
						this.send(DISP_CMD, SYSTEM_NAME, src, "Invalid transfer parameters! Aborted file transfer.");
						// TODO: send special command "transfer abort/error"
					}
				}
            break;
            case FILE_TRANSFER_RESPONSE_CMD:
                String[] resData = data.split(" ", 3);
                
				if (resData.length != 3) {
					ChatServer.log.printf("Incorrect file transfer response\n", cmd);
				} else {
					filename = resData[0];
					if (!resData[2].equals("OK"))
						FileTransferServer.unregisterTransfer(src, dst, filename);
					if (!ChatService.routeData(cmd, src, dst, data)) {
						this.send(DISP_CMD, SYSTEM_NAME, src, "Destination user is not connected!");
					}
				}
            break;
            case FILE_RECV_CMD: // useless?
                if (!ChatService.routeData(cmd, src, dst, data)) {
                    this.send(DISP_CMD, SYSTEM_NAME, src, "Destination user is not connected!");
                }
            break;
            case FILE_SEND_CMD: // useless?
                if (!ChatService.routeData(cmd, src, dst, data)) {
                    this.send(DISP_CMD, SYSTEM_NAME, src, "Destination user is not connected!");
                }
            break;
            default:
                return false;
        };
        // Command was processed
        return true;
    }

    private void quit () {
        ChatServer.log.println("Client quitting.");
        ChatService.routeData(DISP_CMD, SYSTEM_NAME, "", String.format("%s quitted the chat", this.userLogin));
        
        ChatServer.authenticationService.deauthenticate(this.userLogin);
        
        try {
            this.input.close();
            this.output.close();
            this.socket.close();
        } catch (Exception e) { 
            ChatServer.log.println("Error in quit");
            e.printStackTrace();
        }
    }
    
    private String listUsers () {
        StringBuilder out = new StringBuilder();
        for (String login : ChatServer.authenticationService.getAuthenticatedLogins())
            out.append(login).append(',');
        System.out.println("listUsers: " + out);
        if (out.length() > 0)
			out.deleteCharAt(out.length() - 1);
        return out.toString();
    }

}


