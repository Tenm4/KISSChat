package server;

import java.net.*;
import java.io.*;
import java.util.HashMap;

class FileTransferServer implements Runnable {

    /* Serveur fichier liste de transferts à réaliser
     * Client annonce qui il est, et l'ID de transfert
     */
     
    public static final String SERVER_IP = ChatServer.SERVER_IP;
    public static final int FILE_SERVER_PORT = 1112;
    
    private static HashMap<String,FileTransferMetadata> transfers = new HashMap<>();

    public static boolean registerTransfer (String src, String dst, String filename, int filesize) {
        if (src == null || dst == null || filename == null || src.equals(dst)) {
            System.out.println("Could not register file transfer!");
            return false;
        }
        FileTransferMetadata ft = new FileTransferMetadata(src, dst, filename, filesize);
        transfers.put(ft.getId(), ft);
        System.out.println("Registered file transfer of id " + ft.getId());
        return true;
    }
    
    public static boolean unregisterTransfer (String src, String dst, String filename) {
        if (src == null || dst == null || filename == null || src.equals(dst)) {
            System.out.println("Could not unregister file transfer!");
            return false;
        }
        FileTransferMetadata ft = new FileTransferMetadata(src, dst, filename, -1);
        transfers.remove(ft.getId());
        System.out.println("Unregistered file transfer of id " + ft.getId());
        return true;
    }
    
    public static FileTransferMetadata getFileTransferMetadata (String ftId) {
        return transfers.get(ftId);
    }

    public void run () {
        ServerSocket serverSocket = null;
        try { 
            serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(SERVER_IP, FILE_SERVER_PORT));
            ChatServer.log.println("File server started.\n");
            while (true) {
                new Thread(new FileTransferService(serverSocket.accept()), "File service").start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (serverSocket != null) serverSocket.close();
            } catch (IOException e) {}
        }
    }
    
}

class FileTransferService implements Runnable {
    
    private Socket socket;
    private BufferedInputStream in;
    private BufferedOutputStream out;
    
    private FileTransferMetadata ftm;

    public FileTransferService (Socket socket) throws IOException {
        this.socket = socket;
        this.in = new BufferedInputStream(socket.getInputStream());
        this.out = new BufferedOutputStream(socket.getOutputStream());
    }
    
    public FileTransferMetadata getFileTransferMetadata () { return this.ftm; }
    public void setFileTransferMetadata (FileTransferMetadata ftm) { this.ftm = ftm; }
    public BufferedInputStream getInputStream() { return this.in; }
    public BufferedOutputStream getOutputStream() { return this.out; }
    
    public void run () {
        System.out.println("Started file transfer thread.");
        String metadata = null, ftId, login;
        byte[] fileBuffer, initBuffer = new byte[256];
        int filesize, bytesRead;
        
        try {
            bytesRead = this.in.read(initBuffer, 0, 256);
            this.out.write(0x31); this.out.flush();
            metadata = new String(initBuffer, 0, bytesRead);
        
            String[] tmp = metadata.split(":");
            // idTransfer, login
            ftId = FileTransferMetadata.generateFTID(tmp[0], tmp[1], tmp[2]);
            login = tmp[3];
            
            System.out.println("Login: " + login + " FTID: " + ftId);
            this.ftm = FileTransferServer.getFileTransferMetadata(ftId);
            filesize = this.ftm.getFilesize();
            fileBuffer = new byte[filesize];
            
            if (login.equals(tmp[0])) {
                this.ftm.setSrcService(this);
                
                // Wait until other endpoint show up
                while (this.ftm.getState() == 1) {
                    System.out.println("Waiting for file transfer destination...");
                    try { Thread.sleep(500); } catch (InterruptedException e) {}
                }
                
                BufferedOutputStream otherOut = this.ftm.getDstService().getOutputStream();
                
                this.in.read(fileBuffer, 0, filesize);
                otherOut.write(fileBuffer, 0, filesize);
                otherOut.flush();
                
                this.ftm.setFinished();
                System.out.println("Transfer finished.");
            } 
            else if (login.equals(tmp[1])) { 
                this.ftm.setDstService(this);
            
                // Wait until other endpoint show up
                while (this.ftm.getState() == 1) {
                    System.out.println("Waiting for file transfer source...");
                    try { Thread.sleep(500); } catch (InterruptedException e) {}
                }
                
                while (this.ftm.getState() == 2) {
                    System.out.println("Waiting for end of file transfer...");
                    try { Thread.sleep(500); } catch (InterruptedException e) {}
                }
            } 
            else System.out.println("Error: neither source nor destination! Aborting.");
        
        } catch (IOException e) {
            System.out.println("Error during file transfer.");
            e.printStackTrace();
            return;
        } 
        finally {
            try {
                if (this.out != null) {
                    this.out.write(0x31);
                    this.out.flush();
                    this.out.close();
                }
                if (this.in != null) this.in.close();
                if (this.socket != null) this.socket.close();
            } catch (IOException e) {}
        }
    }
}

class FileTransferMetadata {
    
    private String id;
    private String src;
    private String dst;
    private String filename;
    private int filesize;
    private FileTransferService srcService;
    private FileTransferService dstService;
    /* State values:
     * - 0 error
     * - 1 waiting for endpoints
     * - 2 started
     * - 3 ended
     */
    private int state;
    
    public FileTransferMetadata (String src, String dst, String filename, int filesize) {
        this.id = generateFTID(src, dst, filename);
        this.src = src;
        this.dst = dst;
        this.filename = filename;
        this.filesize = filesize;
        this.state = 1;
        this.srcService = null;
        this.dstService = null;
    }
    
    public static String generateFTID (String src, String dst, String filename) {
        return String.format("%s:%s:%s", src, dst, filename);
    }
    
    public String getId () { return this.id; }
    public String getSrc () { return this.src; }
    public String getDst () { return this.dst; }
    public String getFilename () { return this.filename; }
    public int getFilesize () { return this.filesize; }
    public FileTransferService getSrcService () { return this.srcService; }
    public void setSrcService (FileTransferService ss) { this.srcService = ss; }
    public FileTransferService getDstService () { return this.dstService; }
    public void setDstService (FileTransferService ds) { this.dstService = ds; }
    public int getState () {
        if (this.state == 1 && this.srcService != null && this.dstService != null)
            this.state = 2;
        return this.state;
    }
    public void setFinished () { this.state = 3; }
}
