package server;

import java.net.*;
import java.io.*;

public class ChatServer {

    public static String SERVER_IP;
    public static final int CHAT_SERVER_PORT = 1111;
    public static final int FILE_SERVER_PORT = 1112;
    public static int MAX_SIMULTANEOUS_CLIENTS;
    
    public static String logFilename;    
    public static PrintStream log;
    public static AuthenticationService authenticationService;

    private static void runChatServer () {
        ServerSocket serverSocket = null;
        try { 
            serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(SERVER_IP, CHAT_SERVER_PORT));
            ChatServer.log.println("Chat server started.");
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ChatService worker = new ChatService(clientSocket);
                worker.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (serverSocket != null) serverSocket.close();
            } catch (IOException e) {}
        }
    }
    
    private static void runFileServer () {
        new Thread(new FileTransferServer(), "File server").start();
    }
    
    private static void parseArgs (String[] args) throws Exception {
        // port, max clients, log dest, auth
        // TODO: config file name
        int port = 1111, maxClients = 2;
        String ip = "127.0.0.1", logDestination = "", authMethods = "smartcard,password,null";
        
        switch (args.length) {
            case 4: authMethods = args[3];
            case 3: logDestination = args[2];
            case 2: maxClients = Integer.parseInt(args[1]);
            case 1: ip = args[0];
        }
        
        SERVER_IP = ip;
        MAX_SIMULTANEOUS_CLIENTS = maxClients;
        if (logDestination.equals("")) {
            logFilename = "stdout";
            log = System.out;
        } else {
            logFilename = logDestination;
            log = new PrintStream(logDestination);
        }
        ChatServer.log.printf("[IP %s] [Ports %d,%d] [Max clients %d] [Log %s]\n", SERVER_IP, CHAT_SERVER_PORT, FILE_SERVER_PORT, MAX_SIMULTANEOUS_CLIENTS, (logFilename.equals("") ? "stdout" : logFilename));
        authenticationService = new AuthenticationService(authMethods.split(","));
    }

    public static void main (String[] args) throws Exception {
        ChatServer.parseArgs(args);
        
        runFileServer();
        runChatServer();
        
    } 
}
