package server;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import opencard.core.util.HexString;
import javax.xml.bind.DatatypeConverter;

import java.math.BigInteger;
import java.util.Date;
import java.util.Random;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Security;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import javax.crypto.ShortBufferException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.crypto.DataLengthException;

public class AuthenticationService { 
    // TODO: prevent simultaneous connections of same user
    
    private HashMap<String,AuthenticationStrategy> authenticationMethods;
    
    private HashMap<String, User> users;

    public AuthenticationService (String[] authMethods) {
        this.users = new HashMap<>();
        this.authenticationMethods = new HashMap<>();
        this.authenticationMethods.put("null", new NullAuthentication(this));
        this.authenticationMethods.put("password", new PasswordAuthentication(this));
        this.authenticationMethods.put("smartcard", new SmartcardChallengeAuthentication(this));
        
        for (String method : authMethods)
            this.authenticationMethods.get(method).setActivated(true);
        ChatServer.log.printf("Authentication methods: %s\n", Arrays.toString(authMethods));
    }
    
    public List<String> getActivatedAuthenticationMethods () { // TODO: null gets added ??
        List<String> authMethods = new ArrayList<>();
        for (String authMethod : this.authenticationMethods.keySet())
            if (this.authenticationMethods.get(authMethod).isActivated())
                authMethods.add(authMethod);
        return authMethods;
    }

    public List<String> getLogins () {
        List<String> logins = new ArrayList<>();
        for (String login : this.users.keySet())
            logins.add(login);
        return logins;
    }
    
    public List<String> getAuthenticatedLogins () {
        List<String> logins = new ArrayList<>();
        for (String login : this.users.keySet())
            if (this.users.get(login).isAuthenticated())
                logins.add(login);
        ChatServer.log.println("getAuthenticatedLogins: " + logins.size());
        return logins;
    }

    private synchronized User registerUser (String login) { // TODO throws USEREXISTS exception
        //if (this.userExists(login))
		User user = new User(login);
        this.users.put(login, user);
        return user;
    }

    private synchronized void unregisterUser (String login) {
        this.users.remove(login);
    }

    public boolean userExists (String login) {
        return this.users.containsKey(login);
    }
    
    public boolean userAuthenticated (String login) {
        return this.userExists(login) && this.users.get(login).isAuthenticated();
    }

    public String authenticate (ChatService chatService, String authMethod) throws QuittedException {
    
        List<String> authMethods = this.getActivatedAuthenticationMethods();
        
		if (authMethod == null) {
			if (authMethods.size() == 0)
				return null;
			else if (authMethods.size() == 1)
				authMethod = authMethods.get(0);
			else {
				do {
					chatService.send(chatService.AUTH_TYPE_CMD, String.join(",", authMethods));
					authMethod = chatService.recvChat();
				} while (!authMethods.contains(authMethod));
			}
		}
        chatService.send(ChatService.AUTH_STATUS_CMD, "METHOD," + authMethod);
        
		User user = null;
		try {
			user = this.authenticationMethods.get(authMethod).authenticate(chatService);
        } catch (Exception e) { e.printStackTrace(); }
		if (user == null) {
            chatService.send(ChatService.QUIT_CMD, "Fatal authentication error. Quitting...");
            throw new QuittedException();
        }
        // Notify user is successfully logged in
        chatService.send(ChatService.AUTH_STATUS_CMD, ChatService.SYSTEM_NAME, user.getLogin(), "SUCCESS," + user.getLogin());
        
        return user.getLogin();
    }
    
    public boolean deauthenticate (String login) {
        if (login == null) return false; 
        ChatServer.log.println("Deauthenticating: " + login);
        this.users.get(login).setAuthenticated(false);
        return true;
    }

    abstract class AuthenticationStrategy {

        protected boolean activated;
        protected AuthenticationService authService; 
        
        public AuthenticationStrategy (AuthenticationService authService) {
            this.authService = authService;
            this.activated = false;
        }
        
        public boolean isActivated () {
            return this.activated;
        }
        
        public void setActivated (boolean activated) {
            this.activated = activated;
        }

        public abstract User authenticate (ChatService chatService) throws QuittedException;
        
    }

    class NullAuthentication extends AuthenticationStrategy {
        
        public NullAuthentication (AuthenticationService authService) {
            super(authService);
        }
        
        public User authenticate (ChatService chatService) throws QuittedException {
            String login;
            User user;
        
            do {
                chatService.send(chatService.USER_CMD, "");
                login = chatService.recvChat();
                
                if (this.authService.userExists(login)) {
                    user = this.authService.users.get(login);
                } else {
                    user = this.authService.registerUser(login);
                    ChatServer.log.printf("Registered: %s\n", login);
                    chatService.send("Registration successful.");
                }
				user.setAuthenticated(true);
            } while (!user.isAuthenticated());
            
            return user;
        }
        
        public String toString () {
            return "null";
        }
    }

    class PasswordAuthentication extends AuthenticationStrategy {
        
        public PasswordAuthentication (AuthenticationService authService) {
            super(authService);
        }
        
        public User authenticate (ChatService chatService) throws QuittedException {
            String login, password;
            User user;
        
            do {
                chatService.send(chatService.USER_CMD, "");
                login = chatService.recvChat();
                chatService.send(chatService.PASS_CMD, "");
                password = chatService.recvChat();
                
                if (this.authService.userExists(login) && this.authService.users.get(login).getPassword() != null) {
                    user = this.authService.users.get(login);
                    user.setAuthenticated(user.getPassword().equals(password));
                    if (!user.isAuthenticated()) {
                        ChatServer.log.println("Authentication failed: wrong password");
                        chatService.send("Authentication failed: wrong password");
                    } else {
                        ChatServer.log.printf("Authenticated: %s %s\n", login, password);
                        chatService.send("Authentication successful.");
                    }
                } else {
					if (this.authService.userExists(login))
						user = this.authService.users.get(login);
					else user = this.authService.registerUser(login);
					
					user.setAuthenticated(true);
					ChatServer.log.printf("setPassword: " + user.setPassword(password) + "\n");
                    ChatServer.log.printf("Registered: %s %s\n", login, password);
                    chatService.send("Registration successful.");
                }
            } while (!user.isAuthenticated());
            
            return user;
        }
        
        public String toString () {
            return "password";
        }
    }
    
    class SmartcardChallengeAuthentication extends AuthenticationStrategy {
        
		private final byte[] modulusBytes = new byte[] {
		(byte)0x90,(byte)0x08,(byte)0x15,(byte)0x32,(byte)0xb3,(byte)0x6a,(byte)0x20,(byte)0x2f,
		(byte)0x40,(byte)0xa7,(byte)0xe8,(byte)0x02,(byte)0xac,(byte)0x5d,(byte)0xec,(byte)0x11,
		(byte)0x1d,(byte)0xfa,(byte)0xf0,(byte)0x6b,(byte)0x1c,(byte)0xb7,(byte)0xa8,(byte)0x39,
		(byte)0x19,(byte)0x50,(byte)0x9c,(byte)0x44,(byte)0xed,(byte)0xa9,(byte)0x51,(byte)0x01,
		(byte)0x0f,(byte)0x11,(byte)0xd6,(byte)0xa3,(byte)0x60,(byte)0xa7,(byte)0x7e,(byte)0x95,
		(byte)0xa2,(byte)0xfa,(byte)0xe0,(byte)0x8d,(byte)0x62,(byte)0x5b,(byte)0xf2,(byte)0x62,
		(byte)0xa2,(byte)0x64,(byte)0xfb,(byte)0x39,(byte)0xb0,(byte)0xf0,(byte)0x6f,(byte)0xa2,
		(byte)0x23,(byte)0xae,(byte)0xbc,(byte)0x5d,(byte)0xd0,(byte)0x1a,(byte)0x68,(byte)0x11,
		(byte)0xa7,(byte)0xc7,(byte)0x1b,(byte)0xda,(byte)0x17,(byte)0xc7,(byte)0x14,(byte)0xab,
		(byte)0x25,(byte)0x92,(byte)0xbf,(byte)0xcc,(byte)0x81,(byte)0x65,(byte)0x7a,(byte)0x08,
		(byte)0x90,(byte)0x59,(byte)0x7f,(byte)0xc4,(byte)0xf9,(byte)0x43,(byte)0x9c,(byte)0xaa,
		(byte)0xbe,(byte)0xe4,(byte)0xf8,(byte)0xfb,(byte)0x03,(byte)0x74,(byte)0x3d,(byte)0xfb,
		(byte)0x59,(byte)0x7a,(byte)0x56,(byte)0xa3,(byte)0x19,(byte)0x66,(byte)0x43,(byte)0x77,
		(byte)0xcc,(byte)0x5a,(byte)0xae,(byte)0x21,(byte)0xf5,(byte)0x20,(byte)0xa1,(byte)0x22,
		(byte)0x8f,(byte)0x3c,(byte)0xdf,(byte)0xd2,(byte)0x03,(byte)0xe9,(byte)0xc2,(byte)0x38,
		(byte)0xe7,(byte)0xd9,(byte)0x38,(byte)0xef,(byte)0x35,(byte)0x82,(byte)0x48,(byte)0xb7//16
		};
		
		private final byte[] pubExponentBytes = new byte[] {
		(byte)0x01,(byte)0x00,(byte)0x01
		};
		
		// cipher input data
		private final byte[] inC = new byte[] {
		(byte)0x01,(byte)0x02,(byte)0x03,(byte)0x04,(byte)0x05,(byte)0x06,(byte)0x07,(byte)0x08,
		(byte)0x09,(byte)0x0A,(byte)0x0B,(byte)0x0C,(byte)0x0D,(byte)0x0E,(byte)0x0F,(byte)0x10,
		(byte)0x11,(byte)0x12,(byte)0x13,(byte)0x14,(byte)0x15,(byte)0x16,(byte)0x17,(byte)0x18,
		(byte)0x19,(byte)0x1A,(byte)0x1B,(byte)0x1C,(byte)0x1D,(byte)0x1E,(byte)0x1F,(byte)0x20,
		(byte)0x21,(byte)0x22,(byte)0x23,(byte)0x24,(byte)0x25,(byte)0x26,(byte)0x27,(byte)0x28,
		(byte)0x29,(byte)0x2A,(byte)0x2B,(byte)0x2C,(byte)0x2D,(byte)0x2E,(byte)0x2F,(byte)0x30,
		(byte)0x31,(byte)0x32,(byte)0x33,(byte)0x34,(byte)0x35,(byte)0x36,(byte)0x37,(byte)0x38,
		(byte)0x39,(byte)0x3A,(byte)0x3B,(byte)0x3C,(byte)0x3D,(byte)0x3E,(byte)0x3F,(byte)0x40,
		(byte)0x41,(byte)0x42,(byte)0x43,(byte)0x44,(byte)0x45,(byte)0x46,(byte)0x47,(byte)0x48,
		(byte)0x49,(byte)0x4A,(byte)0x4B,(byte)0x4C,(byte)0x4D,(byte)0x4E,(byte)0x4F,(byte)0x50,
		(byte)0x51,(byte)0x52,(byte)0x53,(byte)0x54,(byte)0x55,(byte)0x56,(byte)0x57,(byte)0x58,
		(byte)0x59,(byte)0x5A,(byte)0x5B,(byte)0x5C,(byte)0x5D,(byte)0x5E,(byte)0x5F,(byte)0x60,
		(byte)0x61,(byte)0x62,(byte)0x63,(byte)0x64,(byte)0x65,(byte)0x66,(byte)0x67,(byte)0x68,
		(byte)0x69,(byte)0x6A,(byte)0x6B,(byte)0x6C,(byte)0x6D,(byte)0x6E,(byte)0x6F,(byte)0x70,
		(byte)0x71,(byte)0x72,(byte)0x73,(byte)0x74,(byte)0x75,(byte)0x76,(byte)0x77,(byte)0x78,
		(byte)0x79,(byte)0x7A,(byte)0x7B,(byte)0x7C,(byte)0x7D,(byte)0x7E,(byte)0x7F,(byte)0x01
		};
		
        public SmartcardChallengeAuthentication (AuthenticationService authService) {
            super(authService);
        }
		
		private PublicKey getUserPublicKey(String login) throws InvalidKeySpecException, NoSuchAlgorithmException {
			
			byte[] pubKeyData = DatatypeConverter.parseHexBinary(this.authService.users.get(login).getAuthToken());
			
			ChatServer.log.println("pubKeyData: " + DatatypeConverter.printHexBinary(pubKeyData));
			int pubExpLength = pubKeyData[0] & 0xFF, modulusLength = pubKeyData[1] & 0xFF;
			byte[] pubExponentBytes = Arrays.copyOfRange(pubKeyData, 2, 2 + pubExpLength);
			byte[] modulusBytes = Arrays.copyOfRange(pubKeyData, 2 + pubExpLength, 2 + pubExpLength + modulusLength);
			
			String mod_s = DatatypeConverter.printHexBinary( modulusBytes );
			String pub_s = DatatypeConverter.printHexBinary( pubExponentBytes );
			
			// Load the keys from String into BigIntegers
			BigInteger modulus = new BigInteger(mod_s, 16);
			BigInteger pubExponent = new BigInteger(pub_s, 16);
			ChatServer.log.printf("\nPubExp: (%d) %d\n", pubExponentBytes.length, pubExponent);
			ChatServer.log.printf("Modulus: (%d) %d\n", modulusBytes.length, modulus);

			// Create the RSA public key
			PublicKey pub = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus, pubExponent));
			return pub;
		}
		
		private byte[] rsaEncrypt (byte[] plain, PublicKey userKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, 
			IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, DataLengthException, ShortBufferException {
			byte[] ciphered = new byte[plain.length];
			
				Security.addProvider(new BouncyCastleProvider());
				Cipher cipherRSA_NO_PAD = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
				
				ChatServer.log.printf("\nInput: (%d) %s\n", plain.length, DatatypeConverter.printHexBinary(plain));
				cipherRSA_NO_PAD.init(Cipher.ENCRYPT_MODE, userKey);
				cipherRSA_NO_PAD.doFinal(plain, 0, plain.length, ciphered, 0);
			
			return ciphered;
		}
		
		private boolean challenge (ChatService chatService, String login) throws QuittedException {
			String challengeRequest, challengeResponse, clientChallengeResponse;
			byte[] encyptedChallengeToken = new byte[128], challengeToken = inC;
			int iter = 0;
			
			//new Random((new Date()).getTime()).nextBytes(challengeToken);
			do {
				try {
					PublicKey userKey = this.getUserPublicKey(login);
					encyptedChallengeToken = this.rsaEncrypt(challengeToken, userKey);
					iter = 42;
				} catch (NoSuchProviderException|NoSuchAlgorithmException|InvalidKeySpecException
					|InvalidKeyException|IllegalBlockSizeException|NoSuchPaddingException
					|BadPaddingException|DataLengthException|ShortBufferException e) {
					ChatServer.log.println("\n" + e + "\n");
					iter++;
				}
			} while (iter < 1);
			
			challengeRequest = DatatypeConverter.printBase64Binary(encyptedChallengeToken);
			challengeResponse = DatatypeConverter.printBase64Binary(challengeToken);
			ChatServer.log.println("\nChallengeRequest " + challengeRequest);
			ChatServer.log.println("ChallengeExpectedResponse " + challengeResponse);
		
			chatService.send(ChatService.RSA_CHALLENGE_CMD, challengeRequest);
			clientChallengeResponse = chatService.recvChat();
			
			ChatServer.log.println("ClientChallengeResponse " + clientChallengeResponse);
			boolean challengeStatus = challengeResponse.equals(clientChallengeResponse);
			
			return challengeStatus;
		}
		
		private byte[] requestPublicKey (ChatService chatService) throws QuittedException {
			chatService.send(ChatService.RSA_PUBKEY_CMD, "");
			byte[] pk = DatatypeConverter.parseBase64Binary(chatService.recvChat());
			ChatServer.log.println("Recieved public key: " + DatatypeConverter.printHexBinary(pk) + "\n");
			return pk;
		}
        
        public User authenticate (ChatService chatService) throws QuittedException {
            String login;
			User user = null;
			
			chatService.send(chatService.USER_CMD, "");
			login = chatService.recvChat();
			chatService.send(chatService.SMARTCARD_USE_CMD, "");
			if (!chatService.recvChat().equals("OK")) return null;
		
			if (this.authService.userExists(login) && this.authService.users.get(login).getAuthToken() != null) {
				user = this.authService.users.get(login);
				boolean challengeStatus = this.challenge(chatService, login);
				ChatServer.log.printf("\nChallenge status for user %s: %s\n", login, challengeStatus? "OK" : "KO");
				
				user.setAuthenticated(challengeStatus);
				if (!user.isAuthenticated()) {
					ChatServer.log.println("Authentication failed: wrong challenge token");
					chatService.send("Authentication failed: wrong challenge token");
					throw new QuittedException();
				} else {
					ChatServer.log.printf("Authenticated: %s\n", login);
					chatService.send("Authentication successful.");
				}
			} else {
				String pk = DatatypeConverter.printHexBinary(this.requestPublicKey(chatService));
				
				if (this.authService.userExists(login))
					user = this.authService.users.get(login);
				else user = this.authService.registerUser(login);
				
				user.setAuthenticated(true);
				ChatServer.log.printf("setAuthToken: " + user.setAuthToken(pk) + "\n");
				ChatServer.log.printf("Registered: %s\n", login);
				chatService.send("Registration successful.");
			}

			return user;
        }
        
        public String toString () {
            return "smartcard";
        }
    }
}
