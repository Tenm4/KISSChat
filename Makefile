all: build run

build: bin
	javac -cp bin:bin:lib/bcprov-jdk16-1.45.jar -d bin server/*.java

bin:
	mkdir bin

run:
	java -cp bin:lib/bcprov-jdk16-1.45.jar ChatServer

@PHONY: all build run
