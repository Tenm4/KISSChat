package applet;

import javacard.framework.*;
import javacard.security.*;
import javacardx.crypto.*;


public class TheApplet extends Applet {

	final static byte CLA                         = (byte)0x90;
	final static byte INS_GENERATE_RSA_KEY        = (byte)0xF6;
	final static byte INS_GET_PUBLIC_RSA_KEY      = (byte)0xFE;
	final static byte INS_RSA_ENCRYPT             = (byte)0xA0;
	final static byte INS_RSA_DECRYPT             = (byte)0xA2;
	final static byte INS_DES_ENCRYPT             = (byte)0xB0;
	final static byte INS_DES_DECRYPT             = (byte)0xB2;
	static final byte ENTER_PIN					  = (byte)0x01;
	static final byte UPDATE_PIN				  = (byte)0x02;
    
	final static short SW_VERIFICATION_FAILED             = (short)0x6300;
	final static short SW_PIN_VERIFICATION_REQUIRED       = (short)0x6301;
	
	static OwnerPIN pin;
	static boolean pinActive;
	
	// cipher instances
	static Cipher cRSA_NO_PAD_enc, cRSA_NO_PAD_dec, cDES_ECB_NOPAD_enc, cDES_ECB_NOPAD_dec;
	
	// key objects
	static KeyPair keyPair;
	static Key publicRSAKey, privateRSAKey, secretDESKey;

	// cipher key length
	static short cipherRSAKeyLength;
	
	static final byte[] DESKeyBytes = new byte[] { 
		(byte)0xCA,(byte)0xCA,(byte)0xCA,(byte)0xCA,(byte)0xCA,(byte)0xCA,(byte)0xCA,(byte)0xCA};
	
	// RSA Keys section
	// n = modulus
	static final byte[] n = new byte[] {
		(byte)0x90,(byte)0x08,(byte)0x15,(byte)0x32,(byte)0xb3,(byte)0x6a,(byte)0x20,(byte)0x2f,
		(byte)0x40,(byte)0xa7,(byte)0xe8,(byte)0x02,(byte)0xac,(byte)0x5d,(byte)0xec,(byte)0x11,
		(byte)0x1d,(byte)0xfa,(byte)0xf0,(byte)0x6b,(byte)0x1c,(byte)0xb7,(byte)0xa8,(byte)0x39,
		(byte)0x19,(byte)0x50,(byte)0x9c,(byte)0x44,(byte)0xed,(byte)0xa9,(byte)0x51,(byte)0x01,
		(byte)0x0f,(byte)0x11,(byte)0xd6,(byte)0xa3,(byte)0x60,(byte)0xa7,(byte)0x7e,(byte)0x95,
		(byte)0xa2,(byte)0xfa,(byte)0xe0,(byte)0x8d,(byte)0x62,(byte)0x5b,(byte)0xf2,(byte)0x62,
		(byte)0xa2,(byte)0x64,(byte)0xfb,(byte)0x39,(byte)0xb0,(byte)0xf0,(byte)0x6f,(byte)0xa2,
		(byte)0x23,(byte)0xae,(byte)0xbc,(byte)0x5d,(byte)0xd0,(byte)0x1a,(byte)0x68,(byte)0x11,
		(byte)0xa7,(byte)0xc7,(byte)0x1b,(byte)0xda,(byte)0x17,(byte)0xc7,(byte)0x14,(byte)0xab,
		(byte)0x25,(byte)0x92,(byte)0xbf,(byte)0xcc,(byte)0x81,(byte)0x65,(byte)0x7a,(byte)0x08,
		(byte)0x90,(byte)0x59,(byte)0x7f,(byte)0xc4,(byte)0xf9,(byte)0x43,(byte)0x9c,(byte)0xaa,
		(byte)0xbe,(byte)0xe4,(byte)0xf8,(byte)0xfb,(byte)0x03,(byte)0x74,(byte)0x3d,(byte)0xfb,
		(byte)0x59,(byte)0x7a,(byte)0x56,(byte)0xa3,(byte)0x19,(byte)0x66,(byte)0x43,(byte)0x77,
		(byte)0xcc,(byte)0x5a,(byte)0xae,(byte)0x21,(byte)0xf5,(byte)0x20,(byte)0xa1,(byte)0x22,
		(byte)0x8f,(byte)0x3c,(byte)0xdf,(byte)0xd2,(byte)0x03,(byte)0xe9,(byte)0xc2,(byte)0x38,
		(byte)0xe7,(byte)0xd9,(byte)0x38,(byte)0xef,(byte)0x35,(byte)0x82,(byte)0x48,(byte)0xb7
	};

	// e = public exponent
	static final byte[] e = new byte[] {
		(byte)0x01,(byte)0x00,(byte)0x01
	};

	// d = private exponent
	static final byte[] d = new byte[] {
		(byte)0x69,(byte)0xdf,(byte)0x67,(byte)0x25,(byte)0xa3,(byte)0xb8,(byte)0x88,(byte)0xfb,
		(byte)0xf2,(byte)0xfc,(byte)0xf9,(byte)0x90,(byte)0xad,(byte)0x7f,(byte)0x44,(byte)0xbd,
		(byte)0xb8,(byte)0x59,(byte)0xf3,(byte)0x4b,(byte)0xe9,(byte)0x0a,(byte)0x1f,(byte)0x80,
		(byte)0x09,(byte)0x59,(byte)0xb5,(byte)0xe4,(byte)0xfd,(byte)0x06,(byte)0x0e,(byte)0xe3,
		(byte)0x46,(byte)0x5e,(byte)0x88,(byte)0x76,(byte)0x03,(byte)0xe0,(byte)0x5b,(byte)0x2e,
		(byte)0x47,(byte)0x65,(byte)0x3e,(byte)0x96,(byte)0xef,(byte)0x0c,(byte)0x43,(byte)0x79,
		(byte)0xb9,(byte)0x81,(byte)0x9d,(byte)0x21,(byte)0xe5,(byte)0x2c,(byte)0x78,(byte)0x02,
		(byte)0xa9,(byte)0x54,(byte)0x12,(byte)0x66,(byte)0xab,(byte)0x48,(byte)0x1d,(byte)0xe2,
		(byte)0x6e,(byte)0x1d,(byte)0x7d,(byte)0xb2,(byte)0xce,(byte)0x7a,(byte)0x3f,(byte)0xbb,
		(byte)0x34,(byte)0xf2,(byte)0x46,(byte)0x5f,(byte)0x73,(byte)0x7c,(byte)0xba,(byte)0xf8,
		(byte)0xc1,(byte)0x29,(byte)0x97,(byte)0x85,(byte)0x67,(byte)0xdf,(byte)0x82,(byte)0x87,
		(byte)0x89,(byte)0x61,(byte)0x42,(byte)0xcc,(byte)0x1d,(byte)0xcc,(byte)0x03,(byte)0xce,
		(byte)0x41,(byte)0x7d,(byte)0x8f,(byte)0x25,(byte)0xc1,(byte)0x61,(byte)0xfe,(byte)0x06,
		(byte)0x4f,(byte)0x1a,(byte)0xf2,(byte)0x48,(byte)0x55,(byte)0xd8,(byte)0x6e,(byte)0xc6,
		(byte)0x3f,(byte)0x6d,(byte)0xe1,(byte)0xce,(byte)0xa9,(byte)0x28,(byte)0x9e,(byte)0x03,
		(byte)0x2d,(byte)0x74,(byte)0x59,(byte)0x1c,(byte)0xdb,(byte)0x18,(byte)0xb3,(byte)0x41
	};


	protected TheApplet() {
		initPin();
		initRSA();
		initDES();

		register();
	}
	
	private void initPin () {
		// 3 tries 8=Max Size
		this.pin = new OwnerPIN((byte)3,(byte)8);
		// PIN code "0000", offset 0, length 4
		this.pin.update(new byte[]{(byte)0x30,(byte)0x30,(byte)0x30,(byte)0x30},(short)0,(byte)4);
		this.pinActive = false;
	}
	
	private void initRSA () {
		try {
			cipherRSAKeyLength = KeyBuilder.LENGTH_RSA_1024;
			// build RSA pattern keys
			publicRSAKey = KeyBuilder.buildKey(KeyBuilder.TYPE_RSA_PUBLIC, cipherRSAKeyLength, true);
			privateRSAKey = KeyBuilder.buildKey(KeyBuilder.TYPE_RSA_PRIVATE, cipherRSAKeyLength, false);
			// initialize RSA public key
			((RSAPublicKey)publicRSAKey).setModulus(n, (short)0, (short)(cipherRSAKeyLength/8));
			((RSAPublicKey)publicRSAKey).setExponent(e, (short)0, (short)e.length);
			// initialize RSA private key
			((RSAPrivateKey)privateRSAKey).setModulus(n, (short)0, (short)(cipherRSAKeyLength/8));
			((RSAPrivateKey)privateRSAKey).setExponent(d, (short)0, (short)(cipherRSAKeyLength/8));
			// get cipher RSA instance
			cRSA_NO_PAD_enc = Cipher.getInstance((byte)0x0C, false);
			cRSA_NO_PAD_dec = Cipher.getInstance((byte)0x0C, false);
			cRSA_NO_PAD_enc.init(privateRSAKey, Cipher.MODE_ENCRYPT);
			cRSA_NO_PAD_dec.init(privateRSAKey, Cipher.MODE_DECRYPT);
		} catch (Exception e) {
		    ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED); // 0x6A81
	    }
	}
	
	private void initDES () {
		try {
			// build DES keys
			secretDESKey = KeyBuilder.buildKey(KeyBuilder.TYPE_DES, KeyBuilder.LENGTH_DES, false);
			((DESKey)secretDESKey).setKey(DESKeyBytes,(short)0);
			// get cipher DES instance
			cDES_ECB_NOPAD_enc = Cipher.getInstance(Cipher.ALG_DES_ECB_NOPAD, false);
			cDES_ECB_NOPAD_dec = Cipher.getInstance(Cipher.ALG_DES_ECB_NOPAD, false);
			cDES_ECB_NOPAD_enc.init(secretDESKey, Cipher.MODE_ENCRYPT);
			cDES_ECB_NOPAD_dec.init(secretDESKey, Cipher.MODE_DECRYPT);
		} catch (Exception e) {
		    ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED); // 0x6A81
	    }
	}


	public static void install(byte[] bArray, short bOffset, byte bLength) throws ISOException {
		new TheApplet();
	}

	public boolean select() {
		if ( pin.getTriesRemaining() == 0 )
			return false;
		return true;
	}
	
	public void deselect() {
		pin.reset();
	}
	
	void verify (APDU apdu) {
		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();

		if (pinActive && !pin.check(buffer, (byte)5, buffer[4]))
			ISOException.throwIt( SW_VERIFICATION_FAILED );
	}

	public void process(APDU apdu) throws ISOException {
		if (selectingApplet())
			return;

		byte[] buffer = apdu.getBuffer();
		
		if (buffer[ISO7816.OFFSET_CLA] != CLA)
			ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED); // 0x6E00
		
		switch(buffer[ISO7816.OFFSET_INS]) {
			case ENTER_PIN:              enterPIN(apdu); break;
			case UPDATE_PIN:             updatePIN(apdu); break;
			case INS_GENERATE_RSA_KEY:   generateRSAKey(); break;
			case INS_GET_PUBLIC_RSA_KEY: getPublicRSAKey(apdu); break;
			case INS_RSA_ENCRYPT:        cryptRSA(apdu, cRSA_NO_PAD_enc); break;
			case INS_RSA_DECRYPT:        cryptRSA(apdu, cRSA_NO_PAD_dec); break;
			case INS_DES_ENCRYPT:        cryptDES(apdu, cDES_ECB_NOPAD_enc); break;
			case INS_DES_DECRYPT:        cryptDES(apdu, cDES_ECB_NOPAD_dec); break;
			default: ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED); // 0x6A81
		} // SW codes: https://www.win.tue.nl/pinpasjc/docs/apis/jc222/javacard/framework/ISO7816.html
		// 0x6CXX wrong LE (XX = value)
		/*apdu.setIncomingAndReceive();
		buffer[2] = (byte)0x77;
		apdu.setOutgoingAndSend((short)0, (short)4);*/
	}
	

	void generateRSAKey() {
		if (!pin.isValidated() && pinActive)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
		
		keyPair = new KeyPair(KeyPair.ALG_RSA, (short)publicRSAKey.getSize());
		keyPair.genKeyPair();
		publicRSAKey = keyPair.getPublic();
		privateRSAKey = keyPair.getPrivate();
	}


	void cryptRSA (APDU apdu, Cipher cRSA_NO_PAD) {
		if (!pin.isValidated() && pinActive)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
		
		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();
		cRSA_NO_PAD.doFinal(buffer, (short)5, (short)(cipherRSAKeyLength/8), buffer, (short)0);
		apdu.setOutgoingAndSend((short)0, (short)(cipherRSAKeyLength/8));
	}


	void cryptDES (APDU apdu, Cipher cDES_NO_PAD) {
		if (!pin.isValidated() && pinActive)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
		
		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();
		short lc = (short)(buffer[4] & 0xFF);
		// src, offset_src, length, dst, offset_dst
		cDES_NO_PAD.doFinal(buffer, (short)ISO7816.OFFSET_CDATA, lc, buffer, (short)0);
		apdu.setOutgoingAndSend((short)0, lc); // offset, length
	}

	void getPublicRSAKey(APDU apdu) {
		if (!pin.isValidated() && pinActive)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
		
		byte[] buffer = apdu.getBuffer();
		// get the element type and length
		byte keyElement = (byte)(buffer[ISO7816.OFFSET_P2] & 0xFF);
		// check correct type (modulus or exponent)
		if((keyElement != 0x00) && (keyElement != 0x01))
			ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
		// check elements request
		if(keyElement == 0) {
			// retrieve modulus
			buffer[0] = (byte)((RSAPublicKey)publicRSAKey).getModulus(buffer, (short)1);
		} else
			// retrieve exponent
			buffer[0] = (byte)((RSAPublicKey)publicRSAKey).getExponent(buffer, (short)1);
		// send the key element
		apdu.setOutgoingAndSend((short)0, (short)((buffer[0] & 0xFF) + 1)); // buffer[0] is length
	}

	void enterPIN(APDU apdu) {
		verify(apdu);
	}

	void updatePIN(APDU apdu) {
		if (!pin.isValidated() && pinActive)
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
		
		apdu.setIncomingAndReceive();
		byte[] buffer = apdu.getBuffer();
		pin.update(buffer, (short)5, buffer[4]);
	}

}
